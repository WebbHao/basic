<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Matrix Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap-responsive.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/matrix-login.css') }}" />
        <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
        <link href='{{ asset('css/font-face.css') }}' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox">
            <form id="loginform" class="form-vertical" method="POST" action="{{ URL::action('Auth\AuthController@postLogin') }}">
                {!! csrf_field() !!}
                @if (count($errors) > 0)
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
                @endif
                <div class="control-group normal_text"> <h3><img src="{{ asset('img/logo.png') }}" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"></i></span><input type="email" name="email" value="{{ old('email') }}" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="password" id="password" placeholder="Password" >
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left">
                        <a href="{{ URL::action('Auth\PasswordController@getEmail')}}" class="flip-link btn btn-info" id="to-recover-tmp">忘记密码?</a>
                    </span>
                    <span class="pull-right"><a href="{{ URL::action('Auth\AuthController@getRegister') }}" type="submit"  class="btn btn-success" /> 注册</a></span>
                    &nbsp;&nbsp;
                    <span class="pull-right"><button type="submit"  class="btn btn-success" /> 登陆</button></span>
                </div>
            </form>
            {{-- <form id="recoverform" method="POST" action="{{ URL::action('Auth\PasswordController@postEmail') }}" class="form-vertical">
                <p class="normal_text">请输入您的e-mail地址，我们将会发送邮件给你，告诉你如何重置密码.</p>
                    {!! csrf_field() !!}
                    @if (count($errors) > 0)
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                    @endif
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span>
                            <input type="email" id="email" name="email" value="{{ old('email')}}" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="javascript:void(0);" class="flip-link btn btn-success" id="to-login">&laquo; 返回登陆</a></span>
                    <span class="pull-right"><button type="submit" class="btn btn-info"/>重置</button></span>
                </div>
            </form> --}}
        </div>
        
        <script src="{{ asset('js/jquery.min.js') }}"></script>  
        <script src="{{ asset('js/matrix.login.js') }}"></script> 
    </body>

</html>
</form> 
    
