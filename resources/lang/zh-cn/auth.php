<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '该用户的信息不存在，请确认您的输入是否有误.',
    'throttle' => '尝试登录次数过多. 请 :seconds 秒后重试.',

];
