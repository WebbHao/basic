<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码最少需要六个字符',
    'reset'    => '密码重置成功',
    'sent'     => '密码重置连接已经发送到您的邮箱',
    'token'    => '密码重置的令牌无效.',
    'user'     => "当前输入的e-mail地址不存在",

];
